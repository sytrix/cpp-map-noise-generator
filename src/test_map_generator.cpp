#include <SFML/Graphics.hpp>
#include <sys/time.h>
#include <iostream>
#include "map_generator/MapGenerator.hpp"
#include "map_generator/LayerConfig.hpp"

sf::Uint8 *mapToPixel(float *mapArray, int size)
{
    sf::Uint8 *pixels = new sf::Uint8[size * 4];

    for (int i = 0; i < size; i++) {
        int pixelIndex = i * 4;
        float amplitude = mapArray[i] * 192.f;
        if (amplitude > 255) {
            amplitude = 255;
        } else if (amplitude < 0) {
            amplitude = 0;
        }
        pixels[pixelIndex] = amplitude;
        pixels[pixelIndex + 1] = amplitude;
        pixels[pixelIndex + 2] = amplitude;
        pixels[pixelIndex + 3] = 255;
    }

    return pixels;
}

int main(int argc, char *argv[]) 
{
    int width = 800;
    int height = 800;
    LayerConfig **layers = new LayerConfig*[3];
    layers[0] = new LayerConfig(10, 10, 1.0);
    layers[1] = new LayerConfig(25, 25, 0.3);
    layers[2] = new LayerConfig(80, 80, 0.05);


    float *mapArray = MapGenerator::generateMapFromString(width, height, "hello", layers, 3);

    /*
    struct timeval tp1, tp2;
    gettimeofday(&tp1, NULL);
    long int ms1 = tp1.tv_sec * 1000 + tp1.tv_usec / 1000;
   
    for (int i = 0; i < 100; i++) {
        MapGenerator::generateMapFromString(width, height, "hello", layers, 3);
    }

    gettimeofday(&tp2, NULL);
    long int ms2 = tp2.tv_sec * 1000 + tp2.tv_usec / 1000;

    long int timeDiff = ms2 - ms1;
    std::cout << "generation time : " << timeDiff << " ms " << std::endl;
    */
    
    sf::Image image;
    sf::Uint8 *pixels = mapToPixel(mapArray, width * height);

    image.create(width, height, pixels);
    image.saveToFile("images/map-noise.png");

    delete mapArray;
    delete pixels;

    return 0;
}
