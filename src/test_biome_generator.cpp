#include <SFML/Graphics.hpp>
#include <sys/time.h>
#include <iostream>
#include "map_generator/MapGenerator.hpp"
#include "map_generator/LayerConfig.hpp"

sf::Uint8 *mapToPixel(float **mapArray, int nbMapGen, int size, int width, int height, int border)
{
    int realWidth = width - border * 3;
    int realHeight = height - border * 3;
    sf::Uint8 *pixels = new sf::Uint8[realWidth * realHeight * 4];


    sf::Color *mapColors = new sf::Color[nbMapGen];
    
    /*
    for (int i = 0; i < nbMapGen; ++i) {
        mapColors[i].r = rand() % 256;
        mapColors[i].g = rand() % 256;
        mapColors[i].b = rand() % 256;
        mapColors[i].r = 255;
    }

    mapColors[0] = sf::Color(0x29, 0x80, 0xb9);
    mapColors[1] = sf::Color(0x2c, 0x3e, 0x50);
    mapColors[2] = sf::Color(0xf3, 0x9c, 0x12);
    mapColors[3] = sf::Color(0x2e, 0xcc, 0x71);
    mapColors[4] = sf::Color(0xc0, 0x39, 0x2b);
    */

    mapColors[0] = sf::Color(0x22, 0x22, 0x22);
    mapColors[1] = sf::Color(0x45, 0x45, 0x45);
    mapColors[2] = sf::Color(0x6D, 0x6D, 0x6D);
    mapColors[3] = sf::Color(0x80, 0x80, 0x80);
    mapColors[4] = sf::Color(0xA8, 0xA8, 0xA8);

    int cropBegin = width * border;
    int cropEnd = size - width - border * 2;
    for (int i = cropBegin; i < cropEnd; i++) {
        int x = i % width;
        int y = i / width;
        if (x >= border && y >= border && x < width - border * 2 && y < height - border * 2) {
            int pixelIndex = ((x - border) + (y - border) * realWidth) * 4;
            int maxMapIndex = 0;
            float maxMapAmplitude = mapArray[0][i];

            for (int mapIndex = 1; mapIndex < nbMapGen; ++mapIndex) {
                float mapAmplitude = mapArray[mapIndex][i];
                if (mapAmplitude > maxMapAmplitude) {
                    maxMapIndex = mapIndex;
                    maxMapAmplitude = mapAmplitude;
                }
            }

            
            /*
            pixels[pixelIndex] = amplitude;
            pixels[pixelIndex + 1] = amplitude;
            pixels[pixelIndex + 2] = amplitude;
            pixels[pixelIndex + 3] = 255;
            */

            pixels[pixelIndex] = mapColors[maxMapIndex].r;
            pixels[pixelIndex + 1] = mapColors[maxMapIndex].g;
            pixels[pixelIndex + 2] = mapColors[maxMapIndex].b;
            pixels[pixelIndex + 3] = 255;

            for (int j = 0; j < 3; ++j) {
                float amplitude = pixels[pixelIndex + j] * maxMapAmplitude;
                if (amplitude > 255) {
                    amplitude = 255;
                } else if (amplitude < 0) {
                    amplitude = 0;
                }
                pixels[pixelIndex + j] = amplitude;
            }
        }
    }

    delete mapColors;

    return pixels;
}

int main(int argc, char *argv[]) 
{
    int width = 1600;
    int height = 1600;
    LayerConfig **layers = new LayerConfig*[3];
    layers[0] = new LayerConfig(10, 10, 1.0);
    layers[1] = new LayerConfig(25, 25, 0.3);
    layers[2] = new LayerConfig(80, 80, 0.05);

    int nbMapGen = 5;
    float **mapArray = new float*[nbMapGen];
    for (int i = 0; i < nbMapGen; ++i) {
        mapArray[i] = MapGenerator::generateMapFromString(width, height, "hello;" + std::to_string(i), layers, 3);
    }

    /*
    struct timeval tp1, tp2;
    gettimeofday(&tp1, NULL);
    long int ms1 = tp1.tv_sec * 1000 + tp1.tv_usec / 1000;
   
    for (int i = 0; i < 100; i++) {
        MapGenerator::generateMapFromString(width, height, "hello", layers, 3);
    }

    gettimeofday(&tp2, NULL);
    long int ms2 = tp2.tv_sec * 1000 + tp2.tv_usec / 1000;

    long int timeDiff = ms2 - ms1;
    std::cout << "generation time : " << timeDiff << " ms " << std::endl;
    */

    std::map<std::string, int> dico;
    
    
    sf::Image image;
    int border = width / 10;
    sf::Uint8 *pixels = mapToPixel(mapArray, nbMapGen, width * height, width, height, border);

    image.create(width - 3 * border, height - 3 * border, pixels);
    image.saveToFile("images/biome-noise.png");

    for(int i = 0; i < nbMapGen; ++i) {
        delete mapArray[i];
    } 
    delete mapArray;
    delete pixels;

    return 0;
}
