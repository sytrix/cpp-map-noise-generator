#ifndef CHUNK_H
#define CHUNK_H

#include <string>
class World;
class LayerConfig;

class Chunk
{
	public:
		Chunk(World *world, std::string seedStr, int chunkX, int chunkY, LayerConfig **layers, int layerCount);
		~Chunk();

		enum {
			PERLIN_NOISE_LEVEL,
			PERLIN_NOISE_LOCAL_BIOME_A,
			PERLIN_NOISE_LOCAL_BIOME_B,
			PERLIN_NOISE_LOCAL_BIOME_C,
			PERLIN_NOISE_LOCAL_BIOME_D,
			PERLIN_NOISE_LOCAL_BIOME_E,
			PERLIN_NOISE_LOCAL_BIOME_F,


			NB_PERLIN_NOISE_MAP
		};
		
		bool isComplete();
		void generate(LayerConfig **layers, int nbLayer, int chunkSize);
		float *getTerrainLevel();
		char *getTerrainBiome();

	protected:

		float *createLayer(size_t seed, LayerConfig *layer);
		void combination(float **terrains, char *result, int startId, int endId, int chunkSize) ;
		float amplitudeOfChunks(float **amplitudesAtLayer, int layerWidth, int coordX, int coordY);
		void interpolationLayerBorder(LayerConfig *layer, float **amplitudesAtLayer, float *result, int chunkSize);
		void interpolationLayer(LayerConfig *layer, float *amplitudeMap, float *result, int chunkSize);
		
	private:

		

		World *m_world;
		int m_chunkX;
		int m_chunkY;

		float ***m_layersData;
		int m_layerCount;

		float *m_terrainLevel;
		char *m_terrainBiome;
		
};

#endif
