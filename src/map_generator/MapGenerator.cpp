#include "MapGenerator.hpp"

#include <string>
#include <cmath>
#include "LayerConfig.hpp"

#define AMPLITUDE_PRECISION         10000



inline 
float interpolation(float A, float B, float C, float D, float t, float t2, float t3)
{
    float a = -A / 2.0f + (3.0f*B) / 2.0f - (3.0f*C) / 2.0f + D / 2.0f;
    float b = A - (5.0f*B) / 2.0f + 2.0f*C - D / 2.0f;
    float c = -A / 2.0f + C / 2.0f;
    float d = B;
 
    return a*t3 + b*t2 + c*t + d;
}

float *MapGenerator::generateMapFromString(int width, int height, std::string seedStr, LayerConfig **layers, int layerCount)
{
    std::hash<std::string> hashFn;
    size_t seed = hashFn(seedStr);

    return MapGenerator::generateMapFromString(width, height, seed, layers, layerCount);
}

float *MapGenerator::generateMapFromString(int width, int height, int seed, LayerConfig **layers, int layerCount)
{
    float *result = new float[width * height];
    
    for (int i = 0; i < layerCount; i++) {
        MapGenerator::generateLayer(result, width, height, seed + i, layers[i]);
    }

    return result;
}

void MapGenerator::generateLayer(float *result, int width, int height, size_t seed, LayerConfig *layer) 
{
    srand(seed);

    int layerWidth = layer->getWidth();
    int layerHeight = layer->getHeight();
    float amplitude = layer->getAmplitude();
    int layerSize = layerWidth * layerHeight;

    float *layerSurface = new float[layerSize];

    for (int i = 0; i < layerSize; i++) {
        layerSurface[i] = (float)(rand() % AMPLITUDE_PRECISION) / (float)AMPLITUDE_PRECISION;
    }

    float deltaX = (float)layerWidth / (float)width;
    float deltaY = (float)layerHeight / (float)height;
    int mulX = width / layerWidth;
    int mulY = height / layerHeight;

    int layerWidth2 = layerWidth * 2;
    for (int x = 1; x < layerWidth - 2; x++) {
        for (int y = 1; y < layerHeight - 2; y++) {
            int index = x + y * layerWidth;

            int indexM1 = index - 1;
            int indexP1 = index + 1;
            int indexP2 = index + 2;
            
            float p00 = layerSurface[indexM1 - layerWidth];
            float p01 = layerSurface[index - layerWidth];
            float p02 = layerSurface[indexP1 - layerWidth];
            float p03 = layerSurface[indexP2 - layerWidth];

            float p10 = layerSurface[indexM1];
            float p11 = layerSurface[index];
            float p12 = layerSurface[indexP1];
            float p13 = layerSurface[indexP2];

            float p20 = layerSurface[indexM1 + layerWidth];
            float p21 = layerSurface[index + layerWidth];
            float p22 = layerSurface[indexP1 + layerWidth];
            float p23 = layerSurface[indexP2 + layerWidth];

            float p30 = layerSurface[indexM1 + layerWidth2];
            float p31 = layerSurface[index + layerWidth2];
            float p32 = layerSurface[indexP1 + layerWidth2];
            float p33 = layerSurface[indexP2 + layerWidth2];

            float xfract = 0.f;
            for (int fx = 0; fx < mulX; ++fx) {
                
                float xfract2 = xfract*xfract;
                float xfract3 = xfract2*xfract;
                float col0 = interpolation(p00, p01, p02, p03, xfract, xfract2, xfract3);
                float col1 = interpolation(p10, p11, p12, p13, xfract, xfract2, xfract3);
                float col2 = interpolation(p20, p21, p22, p23, xfract, xfract2, xfract3);
                float col3 = interpolation(p30, p31, p32, p33, xfract, xfract2, xfract3);

                float yfract = 0.f;
                for (int fy = 0; fy < mulX; ++fy) {
                    float yfract2 = yfract*yfract;
                    float yfract3 = yfract2*yfract;

                    float value = interpolation(col0, col1, col2, col3, yfract, yfract2, yfract3);
                    result[x * mulX + fx + (y * mulY + fy) * width] += value * amplitude;
                    yfract += deltaY;
                }

                xfract += deltaX;
            }
        }
    }
    
    delete layerSurface;
    
}


 
