#ifndef MAPGENERATOR_H
#define MAPGENERATOR_H

#include <string>

class LayerConfig;

class MapGenerator
{
	public:
		static float *generateMapFromString(int width, int height, std::string seedStr, LayerConfig **layers, int layerCount);
		static float *generateMapFromString(int width, int height, int seed, LayerConfig **layers, int layerCount);
		
	protected:
		

	private:
		static void generateLayer(float *result, int width, int height, size_t seed, LayerConfig *layer);
};

#endif
