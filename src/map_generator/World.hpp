#ifndef WORLD_H
#define WORLD_H

#include <map>
class Chunk;
class LayerConfig;

class World
{
	public:
		World(std::string seedStr, LayerConfig **layers, int nbLayer, int chunkSize);
		~World();
		
		bool generateChunk(int x, int y);
		Chunk *preGenerateChunk(int x, int y);
		Chunk *getChunk(int chunkX, int chunkY);

	protected:
		
	private:

		std::string chunkKey(int x, int y);

		std::map<std::string, Chunk*> m_chunks;
		std::string m_seedStr;
		LayerConfig **m_layers;
		int m_nbLayer;
		int m_chunkSize;
		
};

#endif
