#include "World.hpp"

#include "Chunk.hpp"



World::World(std::string seedStr, LayerConfig **layers, int nbLayer, int chunkSize) 
: m_seedStr(seedStr), m_layers(layers), m_nbLayer(nbLayer), m_chunkSize(chunkSize)
{

}

World::~World() {
	
    std::map<std::string, Chunk*>::iterator itr; 
    for (itr = m_chunks.begin(); itr != m_chunks.end(); ++itr) { 
        delete itr->second;
    }
    delete m_layers;
}

bool World::generateChunk(int x, int y) 
{
    std::string chunkKey = this->chunkKey(x, y);
    std::map<std::string, Chunk *>::iterator it = m_chunks.find(chunkKey);

    if (it != m_chunks.end()) {
        Chunk *chunk = it->second;
        if (chunk->isComplete()) {
            return false;
        } else {
            chunk->generate(m_layers, m_nbLayer, m_chunkSize);
            return true;
        }
    } else {
        Chunk *chunk = new Chunk(this, m_seedStr + ";" + chunkKey, x, y, m_layers, m_nbLayer);
        chunk->generate(m_layers, m_nbLayer, m_chunkSize);
        m_chunks[chunkKey] = chunk;
        return true;
    }
}

Chunk *World::preGenerateChunk(int x, int y) 
{
    std::string chunkKey = this->chunkKey(x, y);
    std::map<std::string, Chunk *>::iterator it = m_chunks.find(chunkKey);

    if (it != m_chunks.end()) {
        return it->second;
    } else {
        Chunk *chunk = new Chunk(this, m_seedStr + ";" + chunkKey, x, y, m_layers, m_nbLayer);
        m_chunks[chunkKey] = chunk;
        return chunk;
    }
}

Chunk *World::getChunk(int chunkX, int chunkY)
{
    std::string chunkKey = this->chunkKey(chunkX, chunkY);
    std::map<std::string, Chunk *>::iterator it = m_chunks.find(chunkKey);

    if (it != m_chunks.end()) {
        return it->second;
    }

    return nullptr;
}

std::string World::chunkKey(int x, int y)
{
    return std::to_string(x) + ";" + std::to_string(y);
}
