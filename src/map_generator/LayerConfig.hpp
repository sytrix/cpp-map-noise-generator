#ifndef LAYERCONFIG_H
#define LAYERCONFIG_H

class LayerConfig
{
	public:
		LayerConfig(int width, int height, float amplitude);
		~LayerConfig();

		int getWidth();
		int getHeight();
		float getAmplitude();
		
	protected:
		
	private:
		int m_width;
		int m_height;
		float m_amplitude;
		
};

#endif
