#include "LayerConfig.hpp"



LayerConfig::LayerConfig(int width, int height, float amplitude) 
: m_width(width), m_height(height), m_amplitude(amplitude)
{
	
}

LayerConfig::~LayerConfig() 
{
	
}

int LayerConfig::getWidth()
{
    return m_width;
}

int LayerConfig::getHeight()
{
    return m_height;
}

float LayerConfig::getAmplitude()
{
    return m_amplitude;
}

