#include <SFML/Graphics.hpp>
#include <sys/time.h>
#include <iostream>
#include "map_generator/World.hpp"
#include "map_generator/Chunk.hpp"
#include "map_generator/LayerConfig.hpp"

#define CHUNK_SIZE      500
#define NB_BIOME        256

sf::Uint8 *mapLevelToPixel(float *mapArray, int size)
{
    sf::Uint8 *pixels = new sf::Uint8[size * 4];

    for (int i = 0; i < size; i++) {
        int pixelIndex = i * 4;
        float amplitude = mapArray[i] * 192.f;
        if (amplitude > 255) {
            amplitude = 255;
        } else if (amplitude < 0) {
            amplitude = 0;
        }
        pixels[pixelIndex] = amplitude;
        pixels[pixelIndex + 1] = amplitude;
        pixels[pixelIndex + 2] = amplitude;
        pixels[pixelIndex + 3] = 255;
    }

    return pixels;
}

sf::Uint8 *mapBiomeToPixel(char *mapArray, int size, sf::Color *mapColors)
{
    sf::Uint8 *pixels = new sf::Uint8[size * 4];

    for (int i = 0; i < size; i++) {
        int pixelIndex = i * 4;
        int biomeId = mapArray[i];
        pixels[pixelIndex] = mapColors[biomeId].r;
        pixels[pixelIndex + 1] = mapColors[biomeId].g;
        pixels[pixelIndex + 2] = mapColors[biomeId].b;
        pixels[pixelIndex + 3] = 255;
    }

    return pixels;
}

void mapLevelArrayToImageFile(std::string filename, float *mapArray) 
{
    sf::Image image;
    sf::Uint8 *pixels = mapLevelToPixel(mapArray, CHUNK_SIZE * CHUNK_SIZE);

    image.create(CHUNK_SIZE, CHUNK_SIZE, pixels);
    image.saveToFile(filename);

    delete pixels;
}

void mapBiomeArrayToImageFile(std::string filename, char *mapArray, sf::Color *mapColors) 
{
    sf::Image image;
    sf::Uint8 *pixels = mapBiomeToPixel(mapArray, CHUNK_SIZE * CHUNK_SIZE, mapColors);

    image.create(CHUNK_SIZE, CHUNK_SIZE, pixels);
    image.saveToFile(filename);

    delete pixels;
}

void printMap(World *world, int chunkX, int chunkY, int size)
{
    int demi = size / 2;

    for (int y = chunkY - demi; y < chunkY + demi; ++y) {
        std::string line = "";
        for (int x = chunkX - demi; x < chunkX + demi; ++x) {
            Chunk *chunk = world->getChunk(x, y);
            
            if (chunk == nullptr) {
                line += ".";
            } else {
                if (chunk->isComplete()) {
                    line += "x";
                } else {
                    line += "o";
                }
            }
        }

        std::cout << line << std::endl;
    }
    
}

int main(int argc, char *argv[]) 
{
    LayerConfig **layers = new LayerConfig*[3];
    layers[0] = new LayerConfig(5, 5, 1.0);
    layers[1] = new LayerConfig(25, 25, 0.4);
    layers[2] = new LayerConfig(100, 100, 0.1);

    World *world = new World("hello", layers, 3, CHUNK_SIZE);


    sf::Color *mapColors = new sf::Color[NB_BIOME];
    
    for (int i = 0; i < NB_BIOME; ++i) {
        mapColors[i].r = rand() % 256;
        mapColors[i].g = rand() % 256;
        mapColors[i].b = rand() % 256;
        mapColors[i].r = 255;
    }

    sf::Clock clock;
    std::cout << "start generation" << std::endl;
    world->generateChunk(0, 0);
    world->generateChunk(-1, 0);
    world->generateChunk(-1, 1);
    world->generateChunk(0, 1);
    world->generateChunk(-2, 1);
    world->generateChunk(0, -1);
    std::cout << "generation time : " << clock.getElapsedTime().asMilliseconds() << " ms" << std::endl;
    Chunk *chunk1 = world->getChunk(0, 0);
    Chunk *chunk2 = world->getChunk(-1, 0);
    Chunk *chunk3 = world->getChunk(-1, 1);
    Chunk *chunk4 = world->getChunk(0, 1);
    Chunk *chunk5 = world->getChunk(-2, 1);
    Chunk *chunk6 = world->getChunk(0, -1);

    mapLevelArrayToImageFile("images/chunk1-noise.png", chunk1->getTerrainLevel());
    mapLevelArrayToImageFile("images/chunk2-noise.png", chunk2->getTerrainLevel());
    mapLevelArrayToImageFile("images/chunk3-noise.png", chunk3->getTerrainLevel());
    mapLevelArrayToImageFile("images/chunk4-noise.png", chunk4->getTerrainLevel());
    mapLevelArrayToImageFile("images/chunk5-noise.png", chunk5->getTerrainLevel());
    mapLevelArrayToImageFile("images/chunk6-noise.png", chunk6->getTerrainLevel());

    mapBiomeArrayToImageFile("images/chunk1-biome.png", chunk1->getTerrainBiome(), mapColors);
    mapBiomeArrayToImageFile("images/chunk2-biome.png", chunk2->getTerrainBiome(), mapColors);
    mapBiomeArrayToImageFile("images/chunk3-biome.png", chunk3->getTerrainBiome(), mapColors);
    mapBiomeArrayToImageFile("images/chunk4-biome.png", chunk4->getTerrainBiome(), mapColors);
    mapBiomeArrayToImageFile("images/chunk5-biome.png", chunk5->getTerrainBiome(), mapColors);
    mapBiomeArrayToImageFile("images/chunk6-biome.png", chunk6->getTerrainBiome(), mapColors);

    printMap(world, 0, 0, 10);

    
    
    for (int i = 0; i < 3; ++i) {
        delete layers[i];
    }
    delete layers;
    delete world;
    delete mapColors;

    return 0;
}
