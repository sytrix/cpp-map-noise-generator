
FILES_MAP_GENERATOR = \
	obj/map_generator/MapGenerator.o \
	obj/map_generator/LayerConfig.o

FILES_WORLD_MAP_GENERATOR = \
	obj/map_generator/Chunk.o \
	obj/map_generator/World.o \
	obj/map_generator/LayerConfig.o


COMPILER_OPTION = -O3 -Wall

build : bin/test_map_generator bin/test_biome_generator bin/test_chunk_generator

buildobjdir :
	mkdir obj
	mkdir obj/map_generator

cleanobjdir : 
	rm -r obj


bin/test_biome_generator : $(FILES_MAP_GENERATOR) obj/test_biome_generator.o
	g++ $(COMPILER_OPTION) -o $@ -lsfml-graphics -lsfml-system $^ 

obj/test_biome_generator.o : src/test_biome_generator.cpp
	g++ $(COMPILER_OPTION) -o $@ -c $<



bin/test_map_generator : $(FILES_MAP_GENERATOR) obj/test_map_generator.o
	g++ $(COMPILER_OPTION) -o $@ -lsfml-graphics -lsfml-system $^ 

obj/test_map_generator.o : src/test_map_generator.cpp
	g++ $(COMPILER_OPTION) -o $@ -c $<



bin/test_chunk_generator : $(FILES_WORLD_MAP_GENERATOR) obj/test_chunk_generator.o
	g++ $(COMPILER_OPTION) -o $@ -lsfml-graphics -lsfml-system $^ 

obj/test_chunk_generator.o : src/test_chunk_generator.cpp
	g++ $(COMPILER_OPTION) -o $@ -c $<




obj/map_generator/%.o : src/map_generator/%.cpp src/map_generator/%.hpp
	g++ $(COMPILER_OPTION) -o $@ -c $<